"""
A program to approximate an integral using a Monte Carlo method

This could be made faster by using vectorization, however it is
kept as simple as possible for clarity and ease of translation into
other languages 

"""
import math
import numpy
import time
numpoints=512 # number of random sample points
I2d=0.0 	# initialize value
I2dsquare=0.0 	# initialize to allow for calculation of variance
for n in xrange(numpoints):
     x=2.0*numpy.random.uniform()-1.0
     y=2.0*numpy.random.uniform()-1.0
     if (x*x+y*y < 1.0):
        I2d=I2d+1.0
        I2dsquare=I2dsquare+1.0

# we scale the integral by the total area and divide by the number of
# points used
I2d=I2d/numpoints
I2dsquare=I2dsquare/numpoints
EstimError=4*numpy.sqrt( (I2dsquare - I2d**2)/numpoints) # estimated error
I2d=I2d*4
print "Value:  %f" %I2d
print "Error estimate: %f" %EstimError
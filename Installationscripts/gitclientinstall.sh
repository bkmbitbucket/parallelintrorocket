#!/bin/bash

# A script to install personal git client on rocket.hpc.ut.ee

# Load modules
module purge
module load gcc-4.8.1

# Go to home directory
cd $HOME
mkdir gitclient
# get release of git client, check github for latest version
wget https://github.com/git/git/archive/v2.8.1.tar.gz
tar -xvf v2.8.1.tar.gz
cd git-2.8.1/
make configure
./configure --prefix=$HOME/gitclientinstall
make
make install
# add installed git to path so it is found first
sed -i -e '$a export PATH=$HOME/gitclientinstall/bin:$PATH' $HOME/.bashrc

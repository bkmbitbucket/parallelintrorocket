#!/bin/bash

# This script installs and runs OpenLB (www.optilb.com), an open source
# parallel lattice boltzmann program on the Rocket cluster at Tartu �likool
# http://www.hpc.ut.ee/rocket_cluster
#
# The files produced by running the example codes can be visualized in a 
# program such as Paraview (http://www.paraview.org/)
# This program is free to download and use. Some introductory
# video tutorials can be found at:
# http://um3d.dc.umich.edu/learning/

cd $HOME
module purge
module load intel_parallel_studio_xe_2015 
export I_MPI_PMI_LIBRARY=/usr/lib64/libpmi.so

# Get OpenLB 
export OpenLBdir=OpenLBrocketIntel
mkdir $OpenLBdir
cd $OpenLBdir
wget "www.optilb.com/openlb/wp-content/uploads/2017/04/olb-1.1r0.tgz" -O olb-1.1r0.tgz 
tar -xvf olb-1.1r0.tgz 


# Install OpenLB 
cd olb-1.1r0
# Change compilation settings to use MPI on Rocket 
cp Makefile.inc Makefile.inc.original
sed '26 c #CXX             := g++' Makefile.inc > Makefile2.inc
sed '29 c CXX             := mpiicpc' Makefile2.inc > Makefile.inc
sed '46 c #ARPRG           := ar' Makefile.inc > Makefile2.inc
sed '47 c ARPRG           := xiar' Makefile2.inc > Makefile.inc
sed '51 c #PARALLEL_MODE   := OFF' Makefile.inc > Makefile2.inc
sed '52 c PARALLEL_MODE   := MPI' Makefile2.inc > Makefile.inc
sed '59 c #BUILDTYPE       := precompiled' Makefile.inc > Makefile2.inc
sed '60 c BUILDTYPE       := generic' Makefile2.inc > Makefile.inc
rm Makefile2.inc
make
# make an example
cd examples
cd cylinder2d
# compile code
make
# run code 
srun -t 00:30:00 --mem=20000 -N 1 -n 20 cylinder2d 
# compress files
tar -cvf tmp.tar tmp
gzip tmp.tar
cd ..
# Move files back to own computer and visualize

cd cylinder3d
# compile code
make
# run code 
srun -t 00:30:00 --mem=20000 -N 2 -n 20 cylinder3d
# compress files
tar -cvf tmp.tar tmp
gzip tmp.tar
cd ..
# Move files back to own computer and visualize

# Another example to run which produces picture files automatically
cd multiComponent2d
make
# run code
srun -t 00:30:00 --mem=20000  -N 2 -n 20 rayleighTaylor2d
# compress files
tar -cvf tmp.tar tmp
gzip tmp.tar
cd ..
# move files back to own computer

#Last examples
cd bifurcation3d
cd eulerLagrange
make
# run code
srun -t 00:30:00 --mem=20000  -N 2 -n 20 bifurcation3d
# compress files
tar -cvf tmp.tar tmp
gzip tmp.tar
cd ..
cd ..
# move files back to own computer